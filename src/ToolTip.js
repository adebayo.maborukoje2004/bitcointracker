import React, { Component } from 'react';
import T from 'prop-types';
import './ToolTip.css';

class ToolTip extends Component {

  render() {
    const {hover_location, current_point} = this.props;
    const svgLocation = document.getElementsByClassName("linechart")[0].getBoundingClientRect();

    let position = {};
    let width = 100;
    position.width = width + 'px';
    position.left = hover_location + svgLocation.left - (width/2);

    return (
      <div className='hover' style={ position }>
        <div className='date'>{ current_point.d }</div>
        <div className='price'>{current_point.p }</div>
      </div>
    )
  }
}

ToolTip.propTypes = {
  hover_location: T.number,
  current_point: T.object
}
export default ToolTip;
