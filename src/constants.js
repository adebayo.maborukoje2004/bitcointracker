
//Some Utils and Constants are saved here.
var date = new Date();
var day = date.getDate();
var month = date.getMonth();
var year = date.getFullYear()
export const URL = `https://api.coindesk.com/v1/bpi/historical/close.json?start=2019-01-01&end=${year}-${month+1 <= 9 ? '0': ''}${month+1}-${day <= 9 ? '0': ''}${day}`;
export const LINCE_CHART_ITEMS = {
  color: '#2196F3',
  pointRadius: 5,
  svgHeight: 300,
  svgWidth: 900,
  xLabelSize: 20,
  yLabelSize: 80
}