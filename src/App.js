import React, { Component } from 'react';
import moment from 'moment';
import { URL } from './constants'
import LineChart from './LineChart';
import ToolTip from './ToolTip';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      data: null,
      hover_location: null,
      active_point: null
    }
  }
  handleChartHover(hover_location, active_point){
    this.setState({
      hover_location: hover_location,
      active_point: active_point
    })
  }
  componentDidMount(){
    this.getData();
  }
  
  getData() {
    const url = URL
    fetch(url).then( r => r.json())
      .then((result) => {
        const sortedData = [];
        let count = 0;
        for (let date in result.bpi){
          sortedData.push({
            d: moment(date).format('MMM DD'),
            p: result.bpi[date].toLocaleString('us-EN',{ style: 'currency', currency: 'USD' }),
            x: count,
            y: result.bpi[date]
          });
          count++;
        }
        this.setState({
          data: sortedData,
          isLoading: false
        })
      })
      .catch((e) => {
        //TODO handle error
        console.log(e);
      });
  }

  render() {
    const {isLoading, hover_location, active_point} = this.state
    return (
      <div className='container'>
        <div className='row'>
          <h2>Bitcoin Line Graph from Jan 2019 to Date</h2>
        </div>
        <div className='row'>
          <div className='popup'>
            {hover_location ? <ToolTip hover_location={hover_location} current_point={active_point}/> : null}
          </div>
        </div>
        <div className='row'>
          <div className='chart'>
            { !isLoading ?
              <LineChart data={this.state.data} onChartHover={ (x , y) => this.handleChartHover(x, y) } current_point={active_point}/>
              : null }
          </div>
        </div>
        <div className='row footer'>
          <div className='footer-content'>Created by Bayo Maborukoje for Gitlab Assessment</div>
          <div className='footer-content'> Data from <a href="http://www.coindesk.com/price/" style={{textDecoration: 'underline'}}>CoinDesk</a> API. </div>
          <div className='footer-content' >March 4, 2019</div>
        </div>
      </div>

    );
  }
}

export default App;
